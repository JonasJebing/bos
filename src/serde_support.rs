use std::borrow::Borrow;

use crate::{Abos, Bos};
use serde::{Deserialize, Deserializer, Serialize, Serializer};

impl<'de, 'b, B: ?Sized, O, S: ?Sized> Deserialize<'de> for Abos<'b, B, O, S>
where
    O: Deserialize<'de>,
{
    #[inline]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        O::deserialize(deserializer).map(Self::Owned)
    }
}

impl<'de, 'b, B: ?Sized, O, S: ?Sized> Deserialize<'de> for Bos<'b, B, O, S>
where
    O: Deserialize<'de>,
{
    #[inline]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        O::deserialize(deserializer).map(Self::Owned)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Serialize for Abos<'b, B, O, S>
where
    B: Serialize,
    O: Serialize + Borrow<B>,
    S: Serialize + Borrow<B>,
{
    fn serialize<Ser>(&self, serializer: Ser) -> Result<Ser::Ok, Ser::Error>
    where
        Ser: Serializer,
    {
        let b: &B = self.borrow();
        b.serialize(serializer)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Serialize for Bos<'b, B, O, S>
where
    B: Serialize,
    O: Serialize + Borrow<B>,
    S: Serialize + Borrow<B>,
{
    fn serialize<Ser>(&self, serializer: Ser) -> Result<Ser::Ok, Ser::Error>
    where
        Ser: Serializer,
    {
        let b: &B = self.borrow();
        b.serialize(serializer)
    }
}
