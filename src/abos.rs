use std::{
    borrow::{Borrow, BorrowMut, Cow},
    cmp::Ordering,
    ffi::{CStr, CString, OsStr, OsString},
    fmt,
    hash::Hash,
    ops::{Add, AddAssign, Deref},
    path::{Path, PathBuf},
    sync::Arc,
};

/**
**A**tomic **B**orrowed, **O**wned or **S**hared smart pointer.

The enum is currently marked as `non_exhaustive`
and has `'static` bounds
so we may later add a `Static(&'static B)` variant or something similar.
*/
#[non_exhaustive]
pub enum Abos<'b, B, O = B, S = O>
where
    B: 'static + ?Sized,
    O: 'static,
    S: 'static + ?Sized,
{
    /// This variant is **unstable** and only available with the `unstable` feature.
    #[cfg(feature = "unstable")] // reason: B: 'static bound may be inconvenient
    Static(&'static B),
    Borrowed(&'b B),
    Owned(O),
    BorrowedArc(&'b Arc<S>),
    Arc(Arc<S>),
}

impl<'b, B: ?Sized, O, S: ?Sized> Abos<'b, B, O, S> {
    /// Extracts the owned data.
    ///
    /// Clones the data if it is not already owned.
    ///
    /// # Examples
    ///
    /// Calling `into_owned` on a `Abos::Borrowed` clones the inner data.
    /// ```
    /// use bos::{Abos, AbosStr};
    ///
    /// let abos: AbosStr = Abos::Borrowed("Hello!");
    /// let owned: String = abos.into_owned();
    /// assert_eq!(owned, "Hello!");
    /// ```
    ///
    /// Calling `into_owned` on a `Abos::Owned` doesn't clone anything.
    /// ```
    /// use bos::{Abos, AbosStr};
    ///
    /// let abos: AbosStr = Abos::Owned("Hello!".to_owned());
    /// let owned: String = abos.into_owned();
    /// assert_eq!(owned, "Hello!");
    /// ```
    ///
    /// Calling `into_owned` on a `Abos::Arc` only clones the inner data
    /// if the reference count is greater than one.
    /// ```
    /// use bos::{Abos, AbosStr};
    ///
    /// let bos: AbosStr = Abos::Arc("Hello!".to_owned().into());
    /// let owned: String = bos.into_owned();
    /// assert_eq!(owned, "Hello!");
    /// ```
    #[inline]
    pub fn into_owned(self) -> O
    where
        B: ToOwned<Owned = O>,
        S: Clone + Into<O>,
    {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => x.to_owned(),
            Self::Borrowed(x) => x.to_owned(),
            Self::Owned(x) => x,
            Self::BorrowedArc(x) => x.deref().clone().into(),
            Self::Arc(x) => Arc::try_unwrap(x)
                .unwrap_or_else(|x| x.deref().clone())
                .into(),
        }
    }

    /// Acquires a mutable reference to the owned form of the data.
    ///
    /// Clones the data if it is not already owned.
    ///
    /// # Examples
    ///
    /// Calling `to_mut` on an `Abos::Borrowed` clones the inner data.
    /// ```
    /// use bos::{Abos, AbosStr};
    ///
    /// let mut hello: AbosStr = Abos::Borrowed("Hello");
    /// hello.to_mut().push_str(", Barbara!");
    /// assert_eq!(hello, "Hello, Barbara!");
    /// ```
    ///
    /// Calling `to_mut` on an `Abos::Owned` doesn't clone anything.
    /// ```
    /// use bos::{Abos, AbosStr};
    ///
    /// let mut hello: AbosStr = Abos::Owned("Hello".to_owned());
    /// hello.to_mut().push_str(", Barbara!");
    /// assert_eq!(hello, "Hello, Barbara!");
    /// ```
    ///
    /// Calling `to_mut` on an `Abos::Arc` only clones the inner data
    /// if the reference count is greater than one.
    /// ```
    /// use bos::{Abos, AbosStr};
    ///
    /// let mut hello: AbosStr = Abos::Arc("Hello".to_owned().into());
    /// hello.to_mut().push_str(", Barbara!");
    /// assert_eq!(hello, "Hello, Barbara!");
    /// ```
    #[inline]
    pub fn to_mut(&mut self) -> &mut O
    where
        B: ToOwned<Owned = O>,
        S: Clone + BorrowMut<O>,
    {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => {
                *self = Abos::Owned(x.to_owned());
                self.to_mut()
            }
            Self::Borrowed(x) => {
                *self = Abos::Owned(x.to_owned());
                self.to_mut()
            }
            Self::Owned(x) => x,
            Self::BorrowedArc(x) => {
                // avoid changing the reference count by immediately cloning S
                *self = Self::Arc(S::clone(x).into());
                self.to_mut()
            }
            Self::Arc(x) => Arc::make_mut(x).borrow_mut(),
        }
    }

    /**
    Convert borrowed variants so that the new `Abos` has a `'static` lifetime.

    # Examples

    ```rust
    # use bos::AbosStr;
    let b = AbosStr::Borrowed("a");
    let s = b.into_static();
    assert_eq!(s, "a");
    assert_matches::assert_matches!(s, AbosStr::Owned(_));
    ```
    */
    #[inline]
    pub fn into_static(self) -> Abos<'static, B, O, S>
    where
        B: ToOwned<Owned = O>,
    {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => Abos::Static(x),
            Self::Borrowed(x) => Abos::Owned(x.to_owned()),
            Self::Owned(x) => Abos::Owned(x),
            Self::BorrowedArc(x) => Abos::Arc(x.clone()),
            Self::Arc(x) => Abos::Arc(x),
        }
    }

    /// Extracts the shared data.
    ///
    /// Converting `B` or `O` to `Arc<S>` may clone the data.
    #[inline]
    pub fn into_arc(self) -> Arc<S>
    where
        for<'a> &'a B: Into<Arc<S>>,
        O: Into<Arc<S>>,
    {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => x.into(),
            Self::Borrowed(x) => x.into(),
            Self::Owned(x) => x.into(),
            Self::BorrowedArc(x) => x.clone(),
            Self::Arc(x) => x,
        }
    }

    /// Convert [`Self::Arc`] from `Arc<S>` to a new `Arc<O>`.
    ///
    /// This will clone the inner data.
    ///
    /// This is **unstable** and only available with the `unstable` feature.
    #[cfg(feature = "unstable")] // reason: recently added
    #[inline]
    pub fn map_shared_to_owned(self) -> Abos<'b, B, O, O>
    where
        S: Borrow<B>,
        B: ToOwned<Owned = O>,
    {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => Abos::Static(x),
            Self::Borrowed(x) => Abos::Borrowed(x),
            Self::Owned(x) => Abos::Owned(x),
            Self::BorrowedArc(x) => Abos::Arc(x.deref().borrow().to_owned().into()),
            Self::Arc(x) => Abos::Arc(x.deref().borrow().to_owned().into()),
        }
    }

    /// Convert [`Self::Arc`] from `Arc<S>` to a new `Arc<B>`.
    ///
    /// This will clone the inner data.
    ///
    /// This is **unstable** and only available with the `unstable` feature.
    #[cfg(feature = "unstable")] // reason: recently added
    #[inline]
    pub fn map_shared_to_borrowed(self) -> Abos<'b, B, O, B>
    where
        S: Borrow<B>,
        for<'a> &'a B: Into<Arc<B>>,
    {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => Abos::Static(x),
            Self::Borrowed(x) => Abos::Borrowed(x),
            Self::Owned(x) => Abos::Owned(x),
            Self::BorrowedArc(x) => Abos::Arc(x.deref().borrow().into()),
            Self::Arc(x) => Abos::Arc(x.deref().borrow().into()),
        }
    }

    /**
    Convert to a new `Abos` that borrows from `self`.

    # Examples

    ```rust
    # use bos::AbosStr;
    let o = AbosStr::Owned("a".into());
    let b = o.to_borrowed();
    assert_eq!(b, "a");
    assert_matches::assert_matches!(b, AbosStr::Borrowed(_));
    ```

    ```rust
    # use bos::AbosStr;
    let a = AbosStr::Arc(String::from("a").into());
    let b = a.to_borrowed();
    assert_eq!(b, "a");
    assert_matches::assert_matches!(b, AbosStr::BorrowedArc(_));
    ```
    */
    #[inline]
    pub fn to_borrowed(&self) -> Abos<'_, B, O, S>
    where
        O: Borrow<B>,
    {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => Abos::Static(x),
            Self::Borrowed(x) => Abos::Borrowed(x),
            Self::Owned(x) => Abos::Borrowed(x.borrow()),
            Self::BorrowedArc(x) => Abos::BorrowedArc(x),
            Self::Arc(x) => Abos::BorrowedArc(x),
        }
    }

    /**
    Convert [`Abos::Owned`] into [`Abos::Arc`].

    # Examples

    ```rust
    # use bos::AbosStr;
    let o = AbosStr::Owned("a".into());
    let s = o.into_shared();
    assert_eq!(s, "a");
    assert_matches::assert_matches!(s, AbosStr::Arc(_));
    ```
    */
    #[inline]
    pub fn into_shared(self) -> Self
    where
        O: Into<Arc<S>>,
    {
        match self {
            Self::Owned(x) => Self::Arc(x.into()),
            _ => self,
        }
    }

    /// This is **unstable** and only available with the `unstable` feature.
    #[cfg(feature = "unstable")] // reason: unclear if actually needed; `make_shared` and `clone` can be used instead.
    #[inline]
    pub fn clone_shared(&mut self) -> Self
    where
        O: Default + Clone + Into<Arc<S>>,
    {
        self.internal_make_shared();
        self.clone()
    }

    /**
    Change [`Abos::Owned`] into [`Abos::Arc`].

    # Examples

    ```rust
    # use bos::AbosStr;
    let mut a = AbosStr::Owned("a".into());
    a.make_shared();
    assert_eq!(a, "a");
    assert_matches::assert_matches!(a, AbosStr::Arc(_));
    ```
    */
    #[inline]
    pub fn make_shared(&mut self)
    where
        O: Default + Into<Arc<S>>,
    {
        self.internal_make_shared()
    }

    #[inline]
    fn internal_make_shared(&mut self)
    where
        O: Default + Into<Arc<S>>,
    {
        if let Self::Owned(x) = self {
            let x = std::mem::take(x);
            *self = Self::Arc(x.into());
        }
    }

    #[inline]
    fn internal_borrow(&self) -> &B
    where
        O: Borrow<B>,
        S: Borrow<B>,
    {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => x,
            Self::Borrowed(x) => x,
            Self::Owned(x) => x.borrow(),
            Self::BorrowedArc(x) => x.deref().deref().borrow(),
            Self::Arc(x) => x.deref().borrow(),
        }
    }
}

#[cfg(feature = "lifetime")]
impl<'b, B: ?Sized, O, S: ?Sized> lifetime::IntoStatic for Abos<'b, B, O, S>
where
    B: ToOwned<Owned = O>,
{
    type Static = Abos<'static, B, O, S>;

    fn into_static(self) -> Abos<'static, B, O, S> {
        self.into_static()
    }
}

#[cfg(feature = "lifetime")]
impl<'r, 'b, B: ?Sized, O, S: ?Sized> lifetime::ToBorrowed for &'r Abos<'b, B, O, S>
where
    O: Borrow<B>,
{
    type Borrowed = Abos<'r, B, O, S>;

    fn to_borrowed(self) -> Abos<'r, B, O, S> {
        self.to_borrowed()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized, Rhs> Add<Rhs> for Abos<'b, B, O, S>
where
    O: Add<Rhs>,
    B: ToOwned<Owned = O>,
    S: Clone + Into<O>,
{
    type Output = <O as Add<Rhs>>::Output;

    #[inline]
    fn add(self, rhs: Rhs) -> <O as Add<Rhs>>::Output {
        self.into_owned() + rhs
    }
}

impl<'b, B: ?Sized, O, S: ?Sized, Rhs> AddAssign<Rhs> for Abos<'b, B, O, S>
where
    O: AddAssign<Rhs>,
    B: ToOwned<Owned = O>,
    S: Clone + BorrowMut<O>,
{
    #[inline]
    fn add_assign(&mut self, rhs: Rhs) {
        *self.to_mut() += rhs;
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> AsRef<B> for Abos<'b, B, O, S>
where
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn as_ref(&self) -> &B {
        self.internal_borrow()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Borrow<B> for Abos<'b, B, O, S>
where
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn borrow(&self) -> &B {
        self.internal_borrow()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Deref for Abos<'b, B, O, S>
where
    O: Borrow<B>,
    S: Borrow<B>,
{
    type Target = B;

    #[inline]
    fn deref(&self) -> &Self::Target {
        self.internal_borrow()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Clone for Abos<'b, B, O, S>
where
    O: Clone,
{
    #[inline]
    fn clone(&self) -> Self {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => Self::Static(x),
            Self::Borrowed(x) => Self::Borrowed(x),
            Self::Owned(x) => Self::Owned(x.clone()),
            Self::BorrowedArc(x) => Self::BorrowedArc(x),
            Self::Arc(x) => Self::Arc(x.clone()),
        }
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Default for Abos<'b, B, O, S>
where
    O: Default,
{
    /// Creates an owned Abos with the default value for the contained owned type.
    #[inline]
    fn default() -> Self {
        Self::Owned(O::default())
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> fmt::Debug for Abos<'b, B, O, S>
where
    B: fmt::Debug,
    O: fmt::Debug,
    S: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            #[cfg(feature = "unstable")]
            Abos::Static(x) => f.debug_tuple("Abos::Static").field(x).finish(),
            Abos::Borrowed(x) => f.debug_tuple("Abos::Borrowed").field(x).finish(),
            Abos::Owned(x) => f.debug_tuple("Abos::Owned").field(x).finish(),
            Abos::BorrowedArc(x) => f.debug_tuple("Abos::BorrowedArc").field(x).finish(),
            Abos::Arc(x) => f.debug_tuple("Abos::Arc").field(x).finish(),
        }
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> fmt::Display for Abos<'b, B, O, S>
where
    B: fmt::Display,
    O: fmt::Display,
    S: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            #[cfg(feature = "unstable")]
            Self::Static(x) => fmt::Display::fmt(x, f),
            Self::Borrowed(x) => fmt::Display::fmt(x, f),
            Self::Owned(x) => fmt::Display::fmt(x, f),
            Self::BorrowedArc(x) => fmt::Display::fmt(x, f),
            Self::Arc(x) => fmt::Display::fmt(x, f),
        }
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Hash for Abos<'b, B, O, S>
where
    B: Hash,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.internal_borrow().hash(state)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialEq<&B> for Abos<'b, B, O, S>
where
    B: PartialEq,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn eq(&self, other: &&B) -> bool {
        self.internal_borrow() == *other
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialEq<B> for Abos<'b, B, O, S>
where
    B: PartialEq,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn eq(&self, other: &B) -> bool {
        self.internal_borrow() == other
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialEq for Abos<'b, B, O, S>
where
    B: PartialEq,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.internal_borrow() == other.internal_borrow()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Eq for Abos<'b, B, O, S>
where
    B: Eq,
    O: Borrow<B>,
    S: Borrow<B>,
{
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialOrd<&B> for Abos<'b, B, O, S>
where
    B: PartialOrd,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn partial_cmp(&self, other: &&B) -> Option<Ordering> {
        self.internal_borrow().partial_cmp(*other)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialOrd<B> for Abos<'b, B, O, S>
where
    B: PartialOrd,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn partial_cmp(&self, other: &B) -> Option<Ordering> {
        self.internal_borrow().partial_cmp(other)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialOrd for Abos<'b, B, O, S>
where
    B: PartialOrd,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.internal_borrow().partial_cmp(other.internal_borrow())
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Ord for Abos<'b, B, O, S>
where
    B: Ord,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.internal_borrow().cmp(other.internal_borrow())
    }
}

impl<'b, B: ToOwned + ?Sized + 'b> From<Cow<'b, B>> for Abos<'b, B, B::Owned, B::Owned> {
    #[inline]
    fn from(p: Cow<'b, B>) -> Self {
        match p {
            Cow::Borrowed(x) => Self::Borrowed(x),
            Cow::Owned(x) => Self::Owned(x),
        }
    }
}

#[cfg(feature = "unstable")] // reason: Static variant is unstable
impl<B: ?Sized, O, S: ?Sized> From<&'static B> for Abos<'static, B, O, S> {
    #[inline]
    fn from(x: &'static B) -> Self {
        Self::Static(x)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> From<Arc<S>> for Abos<'b, B, O, S> {
    #[inline]
    fn from(x: Arc<S>) -> Self {
        Self::Arc(x)
    }
}

impl<'b, T> From<Vec<T>> for Abos<'b, [T], Vec<T>> {
    #[inline]
    fn from(x: Vec<T>) -> Self {
        Self::Owned(x)
    }
}

impl<'b, T> From<Abos<'b, [T], Vec<T>>> for Vec<T>
where
    T: Clone,
    [T]: ToOwned<Owned = Vec<T>>,
    Vec<T>: Clone,
{
    #[inline]
    fn from(x: Abos<'b, [T], Vec<T>>) -> Self {
        x.into_owned()
    }
}

impl<'b> From<String> for Abos<'b, str, String> {
    #[inline]
    fn from(x: String) -> Self {
        Self::Owned(x)
    }
}

impl<'b> From<Abos<'b, str, String>> for String {
    #[inline]
    fn from(x: Abos<'b, str, String>) -> Self {
        x.into_owned()
    }
}

impl<'b> From<CString> for Abos<'b, CStr, CString> {
    #[inline]
    fn from(x: CString) -> Self {
        Self::Owned(x)
    }
}

impl<'b> From<Abos<'b, CStr, CString>> for CString {
    #[inline]
    fn from(x: Abos<'b, CStr, CString>) -> Self {
        x.into_owned()
    }
}

impl<'b> From<OsString> for Abos<'b, OsStr, OsString> {
    #[inline]
    fn from(x: OsString) -> Self {
        Self::Owned(x)
    }
}

impl<'b> From<Abos<'b, OsStr, OsString>> for OsString {
    #[inline]
    fn from(x: Abos<'b, OsStr, OsString>) -> Self {
        x.into_owned()
    }
}

impl<'b> From<PathBuf> for Abos<'b, Path, PathBuf> {
    #[inline]
    fn from(x: PathBuf) -> Self {
        Self::Owned(x)
    }
}

impl<'b> From<Abos<'b, Path, PathBuf>> for PathBuf {
    #[inline]
    fn from(x: Abos<'b, Path, PathBuf>) -> Self {
        x.into_owned()
    }
}
