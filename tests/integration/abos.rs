use bos::AbosStr as Str;

#[test]
fn cmp() {
    assert_eq!(Str::Borrowed("a"), "a");
    assert!(Str::Borrowed("a") >= "a");
}
