mod abos;
mod bos_;

#[test]
fn size_of_bos_str() {
    use bos::{AbosStr, BosStr};
    use std::borrow::Cow;
    use std::mem::size_of;

    dbg!(size_of::<&str>());
    dbg!(size_of::<String>());
    dbg!(size_of::<AbosStr>());
    dbg!(size_of::<BosStr>());
    dbg!(size_of::<Cow<str>>());
    assert_eq!(size_of::<Cow<str>>(), size_of::<AbosStr>());
    assert_eq!(size_of::<Cow<str>>(), size_of::<BosStr>());
}
