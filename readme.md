# bos

Flexible **B**orrowed, **O**wned or **S**hared (B.O.S.) smart pointers.
Like std's Cow but with Rc/Arc and without the ToOwned requirement.

Have a look at the [documentation](https://docs.rs/bos/) for more information.

## Safety

This crate uses `#![forbid(unsafe_code)]`.
We want to keep this crate 100% safe and its dependencies to a minimum.
Currently this crate has no dependencies using unsafe code,
unless you activate the `serde` feature,
which brings in the [serde crate](https://crates.io/crates/serde),
that has some code using `unsafe`.

## License

Licensed under either of [Apache License, Version 2.0](LICENSE-APACHE)
or [MIT license](LICENSE-MIT)
at your option.

### Contribution

Unless you explicitly state otherwise,
any contribution intentionally submitted for inclusion in this crate by you,
as defined in the Apache-2.0 license,
shall be dual licensed as above,
without any additional terms or conditions.
